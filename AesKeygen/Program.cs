﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace AesKeygen
{
    class Program
    {
        static void Main(string[] args)
        {
            using (var aes = AesCryptoServiceProvider.Create())
            {
                aes.GenerateKey();
                aes.GenerateIV();
                Console.WriteLine("AES key:{0} block:{1}", aes.KeySize, aes.BlockSize);
                Console.WriteLine("Key:{0}", Convert.ToBase64String(aes.Key));
                Console.WriteLine("IV:{0}", Convert.ToBase64String(aes.IV));
                if (args.Length > 0)
                {
                    try
                    {
                        var fs = File.AppendText(args[0]);
                        fs.WriteLine("AES key: {0} block:{1}", aes.KeySize, aes.BlockSize);
                        fs.WriteLine("Key:{0}", Convert.ToBase64String(aes.Key));
                        fs.WriteLine("IV:{0}", Convert.ToBase64String(aes.IV));
                        fs.Close();
                    }
                    catch(Exception e)
                    {
                        Console.WriteLine("File writing error. " + e.Message);
                    }
                }
            }
        }
    }
}
