﻿using System;
using System.IO;

namespace SkyIdentity
{
    public static class AuthCookieSerializer
    {
        public static AuthCookie DeserializeCookie(byte[] serializedCookie)
        {
            using (var binaryStream = new MemoryStream(serializedCookie))
            {
                var reader = new BinaryReader(binaryStream);
                // GUID
                var guidBuffer = new byte[16];
                if (reader.Read(guidBuffer, 0, guidBuffer.Length) < 16)
                    return null;
                var guid = new Guid(guidBuffer);
                // Длина строки UserName
                var nameLength = reader.ReadInt32();
                // UserName
                reader.BaseStream.Position++;
                var userName = new string(reader.ReadChars(nameLength));
                // IsPersistent
                var isPersistent = reader.ReadBoolean();
                // ExpirationDate
                var expirationTicks = reader.ReadInt64();
                var expirationDate = new DateTime(expirationTicks);
                // MaxAge
                var maxAgeTicks = reader.ReadInt64();
                var maxAge = new DateTime(maxAgeTicks);
                return new AuthCookie(userName, guid, expirationDate, isPersistent) { MaxAge = maxAge };
            }
        }

        public static byte[] SerializeCookie(AuthCookie cookie)
        {
            using (var binaryStream = new MemoryStream())
            {
                var writer = new BinaryWriter(binaryStream);
                // GUID
                writer.Write(cookie.Guid.ToByteArray());
                // Длина строки UserName
                writer.Write(cookie.UserName.Length);
                // UserName
                writer.Write(cookie.UserName);
                // IsPersistent
                writer.Write(cookie.IsPersistent);
                // ExpirationDate
                writer.Write(cookie.ExpirationDate.Ticks);
                // MaxAge
                writer.Write(cookie.MaxAge.Ticks);
                return binaryStream.ToArray();
            }
        }
    }
}