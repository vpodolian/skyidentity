﻿using System;

namespace SkyIdentity
{
    public class AuthCookie
    {
        public Guid Guid { get; protected set; }
        public DateTime ExpirationDate { get; protected set; }
        public string UserName { get; protected set; }
        public bool IsPersistent { get; protected set; }
        public DateTime MaxAge { get; set; }
        public bool Expired 
        { 
            get 
            {
                if (ExpirationDate == DateTime.MinValue) // Session cookie
                {
                    return MaxAge.ToUniversalTime() <= DateTime.UtcNow;
                }
                // Значения даты сравниваются в UTC, чтобы избежать проблем с переводом на зимнее/летнее время
                return ExpirationDate.ToUniversalTime() <= DateTime.UtcNow;
            }
        }

        public AuthCookie(string userName, Guid guid, DateTime expirationDate, bool persistent)
        {
            UserName = userName;
            Guid = guid;
            ExpirationDate = expirationDate;
            IsPersistent = persistent;
        }

        public AuthCookie(string userName, DateTime expirationDate, bool persistent)
        {
            Guid = Guid.NewGuid();
            UserName = userName;
            ExpirationDate = expirationDate;
            IsPersistent = persistent;
        }
    }
}