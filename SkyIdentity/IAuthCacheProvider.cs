﻿namespace SkyIdentity
{
    public interface IAuthCacheProvider
    {
        void Add(string key, AuthCookie value);
        AuthCookie GetCookie(string key);
        bool Remove(string key);
        void RemoveAll();
        void RemoveUserCookies(string userName);
        void RemoveExpired(string userName);
    }
}
