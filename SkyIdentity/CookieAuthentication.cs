﻿using System;
using System.Configuration;
using System.Linq;
using System.Security.Cryptography;
using System.IO;
using System.Text;

namespace SkyIdentity
{
    public class CookieAuthentication
    {
        protected IAuthCacheProvider CookieCache;

        /// <summary>
        /// Ключ AES
        /// </summary>
        protected static byte[] _key;
        /// <summary>
        /// Вектор инициализации AES
        /// </summary>
        protected static byte[] _vector;

        private const string DefaultCookieName = "SkyAUTH";
        private static string _cookieName;
        public static string CookieName
        {
            get
            {
                return !string.IsNullOrEmpty(_cookieName) ? _cookieName : DefaultCookieName;
            }
            set { _cookieName = value; }
        }

        private const int DefaultMaxAgeDays = 7;
        private static int _maxAge;
        /// <summary>
        /// Максимальное время жизни Cookie в днях
        /// </summary>
        public static int CookieMaxAgeDays
        {
            get
            {
                return _maxAge > 0 ? _maxAge : DefaultMaxAgeDays;
            }
            set { _maxAge = value; }
        }

        private AuthCookie _lastAuthorizedCookie;
        /// <summary>
        /// Возращает имя последнего авторизованного пользователя
        /// </summary>
        public string CurrentUser
        {
            get
            {
                if (_lastAuthorizedCookie == null)
                    return string.Empty;
                return _lastAuthorizedCookie.UserName;
            }
        }

        protected CookieAuthentication() { }

        public CookieAuthentication(IAuthCacheProvider cookieCacheProvider)
        {
            CookieCache = cookieCacheProvider;
            Initialize();
        }

        static CookieAuthentication()
        {
            Initialize();
        }

        static void Initialize()
        {
            if (ConfigurationManager.AppSettings.HasKeys())
            {
                if (ConfigurationManager.AppSettings.AllKeys.Contains("AuthCookieName"))
                    _cookieName = ConfigurationManager.AppSettings["AuthCookieName"];
                if (ConfigurationManager.AppSettings.AllKeys.Contains("AuthCookieMaxAge"))
                    _maxAge = int.Parse(ConfigurationManager.AppSettings["AuthCookieMaxAge"]);
                GetKeys();
            }
        }

        /// <summary>
        /// Получает пароль из настроек для генерации секретного ключа шифрования и вектора инициализации AES
        /// </summary>
        static void GetKeys()
        {
            if (!ConfigurationManager.AppSettings.AllKeys.Contains("AuthCookiePassword"))
                throw new SettingsPropertyNotFoundException("Необходимо задать параметры AuthCookiePassword блоке <appSettings>!");
            var password = ConfigurationManager.AppSettings["AuthCookiePassword"];
            var sha1 = SHA1.Create();
            var salt = sha1.ComputeHash(Encoding.Unicode.GetBytes(password));
            var keyDerivation = new Rfc2898DeriveBytes(password, salt);
            _key = keyDerivation.GetBytes(32);
            _vector = salt.Take(16).ToArray();
        }

        /// <summary>
        /// Шифрует аутентификационные Cookie
        /// </summary>
        protected byte[] Encrypt(AuthCookie cookie)
        {
            var serialized = AuthCookieSerializer.SerializeCookie(cookie);
            using (var aesService = Aes.Create())
            {
                using (var memStream = new MemoryStream())
                {
                    if (aesService == null)
                    {
                        throw new InvalidOperationException("Cannot create Aes service.");
                    }
                    var cryptoStream = new CryptoStream(memStream, aesService.CreateEncryptor(_key, _vector),
                        CryptoStreamMode.Write);
                    cryptoStream.Write(serialized, 0, serialized.Length);
                    cryptoStream.FlushFinalBlock();
                    return memStream.ToArray();
                }
            }

        }

        /// <summary>
        /// Дешифрует Cookie
        /// </summary>
        /// <returns>Объект AuthCookie; null - если не удалось дешифровать данные</returns>
        protected AuthCookie Decrypt(byte[] encryptedCookie)
        {
            using (var aesService = Aes.Create())
            {
                using (var memStream = new MemoryStream())
                {
                    if (aesService == null)
                    {
                        throw new InvalidOperationException("Cannot create Aes service.");
                    }
                    var cryptoStream = new CryptoStream(memStream, aesService.CreateDecryptor(_key, _vector),
                        CryptoStreamMode.Write);
                    cryptoStream.Write(encryptedCookie, 0, encryptedCookie.Length);
                    cryptoStream.FlushFinalBlock();
                    var decryptedData = memStream.ToArray();
                    var cookie = AuthCookieSerializer.DeserializeCookie(decryptedData);
                    return cookie;
                }
            }
        }

        /// <summary>
        /// Проверяет подлинность Cookie
        /// </summary>
        public bool Validate(string cookie)
        {
            if (string.IsNullOrEmpty(cookie))
                throw new ArgumentNullException("cookie");
            // Дешифруем cookie
            var authCookie = Decrypt(Convert.FromBase64String(cookie));
            if (authCookie == null)
                return false;
            // Проверяем ExpirationDate
            if (authCookie.Expired)
            {
                CookieCache.Remove(authCookie.Guid.ToString());
                return false;
            }
            // Проверяем Cookie в кэше
            if (CookieCache.GetCookie(authCookie.Guid.ToString()) == null)
            {
                return false;
            }
            _lastAuthorizedCookie = authCookie;
            return true;
        }

        /// <summary>
        /// Удаляет все Cookie пользователя
        /// </summary>
        /// <param name="userName"></param>
        public void LogOff(string userName)
        {
            CookieCache.RemoveUserCookies(userName);
        }

        /// <summary>
        /// Создает Cookie, добавляет в кэш и возращает в зашифрованном виде
        /// </summary>
        public byte[] CreateEncryptedCookie(AuthCookie authCookie)
        {
            CookieCache.Add(authCookie.Guid.ToString(), authCookie);
            return Encrypt(authCookie);
        }

        /// <summary>
        /// Создает Cookie, добавляет в кэш и возращает в зашифрованном виде
        /// </summary>
        public byte[] CreateEncryptedCookie(string userName, DateTime expirationDate, bool persistent)
        {
            var authCookie = new AuthCookie(userName, expirationDate, persistent);
            if (authCookie.ExpirationDate == DateTime.MinValue)     // Если session cookie, установить max-age
                authCookie.MaxAge = DateTime.Now.AddDays(CookieMaxAgeDays);
            return CreateEncryptedCookie(authCookie);
        }

        /// <summary>
        /// Создает Cookie и добавляет в кэш
        /// </summary>
        public AuthCookie CreateAuthCookie(string userName, DateTime expirationDate, bool persistent)
        {
            var authCookie = new AuthCookie(userName, expirationDate, persistent);
            CookieCache.Add(authCookie.Guid.ToString(), authCookie);
            return authCookie;
        }
    }
}