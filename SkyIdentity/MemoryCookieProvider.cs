﻿using System.Collections.Generic;
using System.Linq;

namespace SkyIdentity
{
    public class MemoryCookieProvider: IAuthCacheProvider
    {
        static readonly Dictionary<string, AuthCookie> Cache = new Dictionary<string, AuthCookie>();

        public void Add(string key, AuthCookie value)
        {
            if (Cache.ContainsKey(key))
                Cache.Remove(key);
            Cache.Add(key, value);
        }

        public AuthCookie GetCookie(string key)
        {
            if (!Cache.ContainsKey(key))
                return null;
            return Cache[key];
        }

        public bool Remove(string key)
        {
            return Cache.Remove(key);
        }

        public void RemoveAll()
        {
            Cache.Clear();
        }

        public void RemoveUserCookies(string userName)
        {
            var userCookies = Cache.Where(c => c.Value.UserName == userName).Select(c => c.Key).ToList();
            RemoveCookies(userCookies);
        }

        public void RemoveExpired(string userName)
        {
            var userExpiredCookies = Cache.Where(c => c.Value.UserName == userName && c.Value.Expired).Select(c => c.Key).ToList();
            RemoveCookies(userExpiredCookies);
        }

        private void RemoveCookies(IList<string> keys)
        {
            for (int i = 0; i < keys.Count; i++)
			{
                Cache.Remove(keys[i]);
			}
        }
    }
}
