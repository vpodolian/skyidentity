﻿using SkyIdentity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xunit;

namespace CookieIdentityTests
{
    public class CookieAuthenticationTests : CookieAuthentication
    {
        [Fact]
        public void Encryption_n_decryption_test()
        {
            var authCookie = new AuthCookie("test", Guid.NewGuid(), DateTime.Now.AddDays(1), false);
            var encryptedCookie = Encrypt(authCookie);
            var decrypted = Decrypt(encryptedCookie);
            Assert.True(authCookie.Guid == decrypted.Guid);
        }

        [Fact]
        public void CreateAuthCookie_creates_cookie_with_specified_parameters()
        {
            CookieCache = new MemoryCookieProvider();
            var userName = "test";
            var date = DateTime.Now.AddHours(1);
            var persistent = false;
            var createdCookie = CreateAuthCookie(userName, date, persistent);
            Assert.True(createdCookie.ExpirationDate == date && createdCookie.UserName == userName && createdCookie.IsPersistent == persistent);
        }

        [Fact]
        public void CreateEncryptedCookie_adds_cookie_to_cache()
        {
            CookieCache = new MemoryCookieProvider();
            var userName = "test";
            var date = DateTime.Now.AddHours(1);
            var persistent = false;
            var authCookie = new AuthCookie(userName, date, persistent);
            CreateEncryptedCookie(authCookie);
            var cookieFromCache = CookieCache.GetCookie(authCookie.Guid.ToString());
            Assert.True(cookieFromCache.Guid == authCookie.Guid);
        }

        [Fact]
        public void Validate_returns_true_if_cache_has_valid_cookie()
        {
            CookieCache = new MemoryCookieProvider();
            var userName = "test";
            var date = DateTime.Now.AddHours(1);
            var persistent = false;
            var encryptedCookie = CreateEncryptedCookie(userName, date, persistent);
            var cookie = Convert.ToBase64String(encryptedCookie);
            Assert.True(Validate(cookie));
        }
    }
}
