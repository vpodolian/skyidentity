﻿using SkyIdentity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xunit;

namespace CookieIdentityTests
{
    public class AuthCookieSerializerTests
    {
        [Fact]
        public void Serialize_n_Deserialize_Cookie_works_rigth()
        {
            var authCookie = new AuthCookie("test", Guid.NewGuid(), DateTime.Now.AddDays(1), false);
            var serializedCookie = AuthCookieSerializer.SerializeCookie(authCookie);
            var deserialized = AuthCookieSerializer.DeserializeCookie(serializedCookie);
            Assert.True(
                authCookie.Guid == deserialized.Guid 
                && authCookie.UserName == deserialized.UserName
                && authCookie.ExpirationDate == deserialized.ExpirationDate
                && authCookie.IsPersistent == deserialized.IsPersistent);
        }
    }
}
