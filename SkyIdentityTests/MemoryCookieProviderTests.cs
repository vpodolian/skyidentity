﻿using SkyIdentity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xunit;

namespace CookieIdentityTests
{
    public class MemoryCookieProviderTests
    {
        [Fact]
        public void RemoveUserCookies_removes_all_user_cookies()
        {
            var userCookie1 = new AuthCookie("test", Guid.NewGuid(), DateTime.Now.AddDays(1), false);
            var userCookie2 = new AuthCookie("test", Guid.NewGuid(), DateTime.Now.AddDays(2), false);
            var memCookie = new MemoryCookieProvider();
            memCookie.Add(userCookie1.Guid.ToString(), userCookie1);
            memCookie.Add(userCookie2.Guid.ToString(), userCookie2);
            memCookie.RemoveUserCookies("test");
            Assert.Null(memCookie.GetCookie(userCookie1.Guid.ToString()));
            Assert.Null(memCookie.GetCookie(userCookie2.Guid.ToString()));
        }

        [Fact]
        public void Add_n_Get_cookie()
        {
            var userName = "test";
            var date = DateTime.Now.AddHours(1);
            var persistent = false;
            var authCookie = new AuthCookie(userName, date, persistent);
            var provider = new MemoryCookieProvider();
            provider.Add(authCookie.Guid.ToString(), authCookie);
            var cookieFromCache = provider.GetCookie(authCookie.Guid.ToString());
            Assert.True(cookieFromCache.Guid == authCookie.Guid);
        }
    }
}
