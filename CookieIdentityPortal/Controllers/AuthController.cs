﻿using SkyIdentity;
using IdentityTest.Filters;
using IdentityTest.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Security.Principal;
using System.Text;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;

namespace IdentityTest.Controllers
{
    [SkyAuth]
    public class AuthController : Controller
    {
        static public Dictionary<string, string> users;

        static AuthController()
        {
            var s = SHA1.Create();
            var hash = s.ComputeHash(Encoding.Unicode.GetBytes("1234"));
            users = new Dictionary<string, string>();
            users.Add("test", Encoding.Unicode.GetString(hash));
        }

        [AllowAnonymous]
        public ActionResult Login(string returnUrl)
        {
            ViewBag.ReturnUrl = returnUrl;
            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        public ActionResult Login(LoginViewModel model, string returnUrl)
        {
            if (ModelState.IsValid)
            {
                var sha1 = SHA1.Create();
                var hash = sha1.ComputeHash(Encoding.Unicode.GetBytes(model.Password));
                if (users.ContainsKey(model.UserName) && Encoding.Unicode.GetString(hash) == users[model.UserName])
                {
                    var authentication = new CookieAuthentication(new MemoryCookieProvider());
                    DateTime expiration = model.RememberMe ? DateTime.Now.AddMinutes(5) : DateTime.MinValue;
                    byte[] encCookie = authentication.CreateEncryptedCookie(model.UserName, expiration, model.RememberMe);
                    var httpCookie = new HttpCookie(CookieAuthentication.CookieName, Convert.ToBase64String(encCookie)) { HttpOnly = true, Expires = expiration};
                    Response.Cookies.Add(httpCookie);
                    return RedirectToLocal(returnUrl);
                }
                else
                {
                    ModelState.AddModelError("", "Invalid username or password.");
                }
            }
            return View(model);
        }

        [AllowAnonymous]
        public ActionResult Register()
        {
            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        public ActionResult Register(RegisterViewModel model)
        {
            if (ModelState.IsValid)
            {
                var sha1 = SHA1.Create();
                var hash = sha1.ComputeHash(Encoding.Unicode.GetBytes(model.Password));
                users.Add(model.UserName, Encoding.Unicode.GetString(hash));

                var authentication = new CookieAuthentication(new MemoryCookieProvider());
                byte[] encCookie = authentication.CreateEncryptedCookie(model.UserName, DateTime.MinValue, false);
                Response.Cookies.Add(new HttpCookie("testAUTH", Encoding.Unicode.GetString(encCookie)));

                return RedirectToAction("Index", "Home");
            }
            return View();
        }

        [SkyAuth]
        public ActionResult LogOff()
        {
            var athentication = new CookieAuthentication(new MemoryCookieProvider());
            athentication.LogOff(User.Identity.Name);
            return RedirectToAction("Index", "Home");
        }

        private ActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
        }
    }
}