﻿using IdentityTest.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SkyIdentity;
using LeviySoft.Attempts;
using System.Security.Principal;

namespace IdentityTest.Filters
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method, Inherited = true, AllowMultiple = true)]
    public class SkyAuth : FilterAttribute, IAuthorizationFilter
    {
        public void OnAuthorization(AuthorizationContext filterContext)
        {

            bool skipAuthorization = filterContext.ActionDescriptor.IsDefined(typeof(AllowAnonymousAttribute), inherit: true)
                                     || filterContext.ActionDescriptor.ControllerDescriptor.IsDefined(typeof(AllowAnonymousAttribute), inherit: true);

            if (skipAuthorization)
                return;

            var cookies = filterContext.HttpContext.Request.Cookies[CookieAuthentication.CookieName];
            if (cookies != null)
            {
                var authentication = new CookieAuthentication(new MemoryCookieProvider());
                if (authentication.Validate(cookies.Value))
                {
                    filterContext.HttpContext.User = new GenericPrincipal(new GenericIdentity(authentication.CurrentUser), new string[0]);
                    return;
                }
            }

            filterContext.Result = new HttpUnauthorizedResult();
        }
    }
}