# Установка
В конфигурационном файле приложения в секцию *appSettings* добавить пароль шифрования *AuthCookiePassword*

```xml
<appSettings>
    <add key="AuthCookiePassword" value="XANB0n7bWW22a8gT"/>
</appSettings>
```

# Как использовать
Основной функционал реализует класс *CookieAuthentication*.

###Валидация пользователя
```csharp
[AttributeUsage(AttributeTargets.Class | AttributeTargets.Method, Inherited = true, AllowMultiple = true)]
public class SkyAuth : FilterAttribute, IAuthorizationFilter
{
    public void OnAuthorization(AuthorizationContext filterContext)
    {

	    bool skipAuthorization = filterContext.ActionDescriptor.IsDefined(typeof(AllowAnonymousAttribute), inherit: true)
	                             || filterContext.ActionDescriptor.ControllerDescriptor.IsDefined(typeof(AllowAnonymousAttribute), inherit: true);

	    if (skipAuthorization)
	        return;

	    var cookies = filterContext.HttpContext.Request.Cookies[CookieAuthentication.CookieName];
        if (cookies != null)
        {
            var authentication = new CookieAuthentication(new MemoryCookieProvider());
            if (authentication.Validate(cookies.Value))
            {
                filterContext.HttpContext.User = new GenericPrincipal(new GenericIdentity(authentication.CurrentUser), new string[0]);
                return;
            }
        }

	    filterContext.Result = new HttpUnauthorizedResult();
    }
}
```

*MemoryCookieProvider* реализует интерфейс *IAuthCacheProvider* - кэш для хранения аутентификационных Cookie.

###Авторизация
```csharp
public ActionResult Login(LoginViewModel model, string returnUrl)
{
	// Проверка проверки имени и пароля.
	// ...
	// Создаем аутентификационные Cookie
	var authentication = new CookieAuthentication(new MemoryCookieProvider());
	// DateTime.MinValue - для Session cookie
	DateTime expiration = model.RememberMe ? DateTime.Now.AddMinutes(5) : DateTime.MinValue;				
	// CreateEncryptedCookie - cоздает шифрованные Cookie с заданным параметрами
	// и сохраняет их в кэше
	byte[] encCookie = authentication.CreateEncryptedCookie(model.UserName, expiration, model.RememberMe);	
	var httpCookie = new HttpCookie(CookieAuthentication.CookieName, Convert.ToBase64String(encCookie)) { HttpOnly = true, Expires = expiration};
	Response.Cookies.Add(httpCookie);
	return View(model);
}
```

###Удаление всех Cookie пользователя
```csharp
[SkyAuth]
public ActionResult LogOff()
{
    var athentication = new CookieAuthentication(new MemoryCookieProvider());
    athentication.LogOff(User.Identity.Name);
    return RedirectToAction("Index", "Home");
}
```

###Интерфейс IAuthCacheProvider
*IAuthCacheProvider* - интерфейс кэша, где хранятся аутентификационные Cookie пользователей. Объект, реализующий *IAuthCacheProvider*, передается в конструктор CookieAuthentication.

###AuthCookie
Класс *AuthCookie* содержит данные аутентификационных Cookie пользователя. *CookieAuthentication* через объект типа *IAuthCacheProvider* помещает и достает  из кэша объекты типа *AuthCookie*